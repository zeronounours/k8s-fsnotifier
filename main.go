package main

import (
	"flag"
	"github.com/fsnotify/fsnotify"
	"golang.org/x/sys/unix"
	"log"
	"os"
	"path"
	"strconv"
	"strings"
	"syscall"
	"time"
)

const retryInterval time.Duration = 2 * time.Second
const maxRetryDuration time.Duration = 5 * time.Minute

var verbose bool

func debug(message string) {
	if verbose {
		log.Print("DEBUG: " + message)
	}
}
func debugf(message string, v ...any) {
	if verbose {
		log.Printf("DEBUG: "+message, v...)
	}
}

const (
	procName = 0
	pidFile  = 1
)

type fsNotifierInfo struct {
	sType       int
	search      string
	pid         int
	process     *os.Process
	signal      syscall.Signal
	mountPoints []string
}

func main() {
	// Parse command-line arguments
	var signalStr string
	var progStr string
	var pidFileStr string
	flag.StringVar(&signalStr, "signal", "SIGHUP", "Signal to send upon file change (Default SIGHUP)")
	flag.StringVar(&progStr, "program", "", "Program to send signal to")
	flag.StringVar(&pidFileStr, "pidfile", "", "Path to the PID file of the program to send signal to")
	flag.BoolVar(&verbose, "v", false, "increase verbosity")
	flag.BoolVar(&verbose, "verbose", false, "increase verbosity")
	flag.Parse()
	mountPoints := flag.Args()

	// check mountPoints
	if len(mountPoints) == 0 {
		log.Fatal("No mount points provided")
	}
	debugf("Provided mount points to track %v\n", mountPoints)

	// intialize the fsNotifierInfo
	notifier := fsNotifierInfo{mountPoints: mountPoints}

	// resolve the PID
	if progStr != "" {
		notifier.sType = procName
		notifier.search = progStr
	} else if pidFileStr != "" {
		notifier.sType = pidFile
		notifier.search = pidFileStr
	} else {
		log.Fatal("No program or PID file provided")
	}

	// resolve the signal from name
	debugf("Provided signal to send %s\n", signalStr)
	notifier.signal = unix.SignalNum(signalStr)
	if notifier.signal == 0 {
		log.Fatal("Invalid signal name")
	}
	debugf("Signal correspond to number %d\n", notifier.signal)

	// start watching in a loop, first time we dont send signal
	startWatcher(notifier, false)
	for {
		startWatcher(notifier, true)
	}
}

func startWatcher(notifier fsNotifierInfo, sendSignalFirst bool) {
	// first resolve the PID
	findPid(&notifier)

	// send signal if asked for - usually it is done when a watched file is
	// modified but the followed PID was terminated
	if sendSignalFirst {
		sendSignal(notifier)
	}

	// Initialize file system watcher
	debug("Start watcher on mount points\n")
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
	}
	defer watcher.Close()

	// Add watch for each mount point
	procPath := path.Join("/proc", strconv.Itoa(notifier.pid))
	rootPath := path.Join(procPath, "root")
	for _, mp := range notifier.mountPoints {
		err := watcher.Add(path.Join(rootPath, mp))
		if err != nil {
			log.Fatalf("Error adding watch for %s: %v", mp, err)
		}
	}

	// Channel to receive filesystem events
	log.Println("Start watching mount points")
	done := make(chan bool)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				debugf("Received fs event %v", event)
				// check proc path existence
				_, err := os.Stat(procPath)
				switch {
				case err != nil:
					log.Printf("Tracked PID (%d) was terminated: restarting", notifier.pid)
					// end the watcher
					done <- true
					return

				case event.Op&fsnotify.Write == fsnotify.Write:
					log.Printf("File modified %s\n", event.Name)
					// File modified, send specified signal
					err := sendSignal(notifier)
					if err != nil {
						log.Println("Error sending signal:", err)
					}

				case event.Op&fsnotify.Remove == fsnotify.Remove:
					log.Printf("File removed %s\n", event.Name)
					// File deleted, send specified signal
					err := sendSignal(notifier)
					if err != nil {
						log.Println("Error sending signal:", err)
					}
					// Try to watch the file again
					// wait a little while prior to adding the watcher again
					time.Sleep(500 * time.Millisecond)
					err = watcher.Add(event.Name)
					if err != nil {
						log.Printf("Error adding watch for %s: %v", event.Name, err)
					}
				}

			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
		}
	}()

	<-done
}

func findPid(notifier *fsNotifierInfo) {
	// resolve the PID
	debug("Resolving PID to send signal to\n")
	if notifier.sType == procName {
		debugf("Resolve PID using program name %s\n", notifier.search)
		notifier.pid = findProgramPidByName(notifier.search)
	} else if notifier.sType == pidFile {
		debugf("Resolve PID using PID file %s\n", notifier.search)
		notifier.pid = findProgramPidByPidFile(notifier.search)
	} else {
		log.Fatalf("Unknown notifier type: %d", notifier.sType)
	}
	debugf("PID to send signal to resolved to %d\n", notifier.pid)
	var err error
	notifier.process, err = os.FindProcess(notifier.pid)
	if err != nil {
		log.Fatalf("Failed to resolve process from PID %d: %v", notifier.pid, err)
	}
}

func findProgramPidByName(program string) int {
	// read all subdirectories in proc
	entries, err := os.ReadDir("/proc/")
	if err != nil {
		log.Fatal(err)
	}

	// try to match program with comm file
	for _, e := range entries {
		content, err := os.ReadFile(path.Join("/proc/", e.Name(), "comm"))
		debugf("Inspect proc entry %s, command is %v\n", e.Name(), content)
		if err == nil && strings.TrimSpace(string(content)) == program {
			pid, err := strconv.Atoi(e.Name())
			if err != nil {
				log.Fatalf("Failed to convert PID directory name to integer: %v", err)
			}
			return pid
		}
	}
	log.Fatalf("Failed to find program: %s", program)
	return 0
}

func findProgramPidByPidFile(pidFile string) int {
	// read all subdirectories in proc
	entries, err := os.ReadDir("/proc/")
	if err != nil {
		log.Fatal(err)
	}

	// the main programe start-up may take some time to create PID file
	for i := time.Duration(0); i < maxRetryDuration; i += retryInterval {
		// try to match program with comm file
		for _, e := range entries {
			path := path.Join("/proc/", e.Name(), "root", pidFile)
			debugf("Inspect proc entry %s\n", e.Name())
			if _, err := os.Stat(path); err == nil {
				debugf("Found PID file for proc entry %s\n", e.Name())
				// PID file exists ==> try to read it, but it may not exist yey
				content, err := os.ReadFile(path)
				if err == nil {
					// convert PID to int and also ensure it match proc entry PID
					// to prevent matching another process which would have the
					// same PID file
					pid, err := strconv.Atoi(strings.TrimSpace(string(content)))
					procpid, err2 := strconv.Atoi(e.Name())
					if err != nil {
						debugf("Failed to read valid PID from it %s\n", string(content))
					} else if err2 != nil {
						debug("Failed to read valid PID from proc entry\n")
					} else if pid != procpid {
						debug("Read PID and proc entry PID do not match\n")
					} else {
						return pid
					}
				} else {
					debug("Failed to read PID file\n")
				}
			}
		}
		log.Println("Failed to find a valid PID file. Retry after a while")
		time.Sleep(retryInterval)
	}
	log.Fatalf("Failed to read pid file: %s", pidFile)
	return 0
}

func sendSignal(notifier fsNotifierInfo) error {
	// Send signal to PID 1
	err := notifier.process.Signal(notifier.signal)
	if err != nil {
		return err
	}
	return nil
}
