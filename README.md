# k8s-fsnotifier

This golang program monitor filesystem files/directory in order to detect
changes and send signal (usually SIGHUP).

This was designed to be used as a sidecar container in kubernetes.

## Getting started

The notifier use a side-car container to track changes to configuration of a
main container.

Just run the side-car container, with `shareProcessNamespace` option. You
should run the program with the signal to be sent, the program to be signaled
and the files/directory to monitor (relative to the main container rootfs).

To configure the signal to send, use `-signal` option. It defaults to `SIGHUP`
which is tho most used signal for config reloading.

### Lookup by name

The first way to manage a service is through its process name, using the option
`-program`.

Here is an sidecar example:

```yaml
---
apiVersion: apps/v1
kind: Deployment

spec:
  template:
    spec:
      shareProcessNamespace: true
      containers:
        - name: mqtt
          image: eclipse-mosquitto:2-openssl
          volumeMounts:
            - mountPath: /mosquitto/data
              name: data
            - mountPath: /mosquitto/config
              name: config
            - mountPath: /mosquitto/config/certs
              name: certs
            - mountPath: /mosquitto/password.txt
              subPath: password.txt
              name: passwords
          # …

        # Here is the side car container
        - name: config-reloader
          image: registry.gitlab.com/zeronounours/k8s-fsnotifier/fsnotifier:latest
          command:
            - "/fsnotifier"
            - "-signal"
            - "SIGHUP"
            - "-program"
            - "mosquitto"
            - "/mosquitto/config"
            - "/mosquitto/config/certs"
            - "/mosquitto/password.txt"
      volumes:
        - name: config
          configMap:
            name: mqtt-config
        - name: passwords
          secret:
            secretName: mqtt-passwords
        - name: certs
          secret:
            secretName: mqtt-tls-cert
```

### Lookup by PID file

Another way to manage a service is through a PID file, using the option
`-pidfile`. The PID file should exists

Here is an sidecar example:

```yaml
---
apiVersion: apps/v1
kind: Deployment

spec:
  template:
    spec:
      shareProcessNamespace: true
      containers:
        - name: squid
          image: squid-proxy
          volumeMounts:
            - mountPath: /etc/squid/
              name: config
          # …

        # Here is the side car container
        - name: config-reloader
          image: registry.gitlab.com/zeronounours/k8s-fsnotifier/fsnotifier:latest
          command:
            - "/fsnotifier"
            - "-signal"
            - "SIGHUP"
            - "-pidfile"
            - "/var/run/squid.pid"
            - "/etc/squid/squid.conf"
      volumes:
        - name: config
          configMap:
            name: squid-conf
```

## Contributing

All contributions are welcomed. See [CONTRIBUTING.md](CONTRIBUTING.md) for more
details.

For developers, you may consider using [pre-commit](https://pre-commit.com) to
ensure a minimal code-quality before pushing. Start by running:

```
go install golang.org/x/lint/golint@latest
pip install pre-commit
pre-commit install
```

## Authors and acknowledgment

Authors:

- Gauthier SEBAUX (a.k.a zeroNounours)

## License

Under MIT license
