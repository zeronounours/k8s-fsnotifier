#!/bin/bash

sigint_received=0

trap 'echo Reveived signal SIGHUP' SIGHUP
trap 'sigint_received=1' SIGINT

echo $$ > /run/signal_handler.pid

echo "Started with PID $$"

while [ "$sigint_received" -eq 0 ]; do
  sleep 10 &
  wait $!
done

echo "Stopped"
