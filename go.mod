module gitlab.com/zeronounours/k8s-fsnotifier

go 1.21.5

require github.com/fsnotify/fsnotify v1.7.0

require golang.org/x/sys v0.4.0 // indirect
