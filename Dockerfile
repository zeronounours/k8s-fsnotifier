ARG BASEIMG=scratch
#####################################
# Build the go binary
#####################################
FROM docker.io/golang:alpine AS build-go
# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git gcc musl-dev

WORKDIR $GOPATH/src/gitlab.com/zeronounours/k8s-fsnotifier/
COPY . .
# Fetch dependencies.
# Using go get.
RUN go get -d -v
# Build the binary.
# since new version of git, golang vcs stamping will fail due to different
# owner ==> need to add safe.directory
RUN git config --global --add safe.directory "$(pwd)" \
	&& CGO_ENABLED=1 GOOS=linux go build -a -ldflags '-linkmode external -extldflags "-static"' -o /go/bin/fsnotifier

#####################################
# Create the final image
#####################################
FROM ${BASEIMG}
# Copy our static executable.
COPY --from=build-go /go/bin/fsnotifier /fsnotifier
# Run the binary.
ENTRYPOINT ["/fsnotifier"]
